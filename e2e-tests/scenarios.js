'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function () {

  it('should automatically redirect to /user when location hash/fragment is empty', function () {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/user");
  });

  describe('user', function () {

    beforeEach(function () {
      browser.get('index.html#!/user');
    });

    it('should render user when user navigates to /user', function () {
      expect(element.all(by.css('[ng-view] span')).first().getText()).
      toMatch(/Fruit and Friends/);
    });

  });
});