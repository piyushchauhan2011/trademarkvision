'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ngAria',
  'ngAnimate',
  'ngMessages',
  'ngMaterial',
  'md.data.table',
  'myApp.user',
  'myApp.shared'
])
// allow DI for use in controllers, unit tests
.constant('_', window._)
// use in views, ng-repeat="x in _.range(3)"
.run(['$rootScope', function ($rootScope) {
    $rootScope._ = window._;
}])
.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/user'});
}]);
