'use strict';

describe('myApp.user module', function () {

  beforeEach(module('myApp.user'));

  describe('user controller', function () {
    var $scope, $controller, lodash, User;
    beforeEach(inject(function (_$controller_, _User_) {
      $scope = {};
      lodash = window._;
      $controller = _$controller_;
      User = _User_;
    }));

    it('should instantiate User controller correctly', function () {
      var userCtrl = $controller('UserCtrl', {
        $scope: $scope,
        _: lodash,
        User: User
      });
      expect(userCtrl).toBeDefined();
    });

  });
});