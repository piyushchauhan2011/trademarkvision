'use strict';

describe('myApp.user module', function () {

  beforeEach(module('myApp.user'));

  describe('user service', function () {
    var User, httpBackend;

    beforeEach(inject(function (_User_, _$httpBackend_) {
      User = _User_;
      httpBackend = _$httpBackend_;
    }));

    it('should get all users correctly', function () {
      var results = [{
        "favoriteFruit": "apple",
        "friends": [{
          "name": "Tamra Hays"
        }, {
          "name": "Rebekah Knox"
        }, {
          "name": "Jenkins Aguilar"
        }, {
          "name": "Tricia Carr"
        }],
        "email": "talley.best@undefined.me",
        "name": {
          "last": "Best",
          "first": "Talley"
        }
      }];
      httpBackend.whenGET('data.json')
        .respond(results);

      var users = [];
      User.getUsers().then(function (response) {
        users = response.data;
      });
      httpBackend.flush();
      expect(users).toEqual(results);
    });

  });
});