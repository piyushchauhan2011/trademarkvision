'use strict';

angular.module('myApp.user')
  .service('User', ['$http', function ($http) {
    this.getUsers = function () {
      return $http.get('data.json');
    }

    this.filter = function (chip, data) {
      var results = [];
      if (chip.type === 'Name') {
        // perform name search
        results = _.filter(data, function (user) {
          return user.name.first.toLowerCase().indexOf(chip.value) !== -1 ||
            user.name.last.toLowerCase().indexOf(chip.value) !== -1;
        });
      } else if (chip.type === 'Email') {
        results = _.filter(data, function (user) {
          return user.email.toLowerCase().indexOf(chip.value) !== -1;
        });
      } else if (chip.type === 'Favorite Fruit') {
        results = _.filter(data, function (user) {
          return user.favoriteFruit.toLowerCase().indexOf(chip.value) !== -1;
        });
      } else if (chip.type === 'Friends') {
        // Search for friends
        results = _.filter(data, function (user) {
          return _.some(user.friends, function (friend) {
            return friend.name.toLowerCase().indexOf(chip.value) !== -1;
          });
        });
      } else {
        // Default filter by name
        results = _.filter(data, function (user) {
          return user.name.first.toLowerCase().indexOf(chip.value) !== -1 ||
            user.name.last.toLowerCase().indexOf(chip.value) !== -1;
        });
      }
      return results;
    }
  }]);