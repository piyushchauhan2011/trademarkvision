'use strict';

angular.module('myApp.user', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.when('/user', {
    templateUrl: 'user/index.html',
    controller: 'UserCtrl'
  });
}])

.controller('UserCtrl', ['$scope', '_', 'User', function ($scope, _, User) {
  var local = []; // holds all the user data

  $scope.users = {
    count: 0,
    data: []
  };

  User.getUsers().then(function (response) {
    var data = response.data;
    local = data;
    $scope.users = {
      count: data.length,
      data: data
    };
  });

  $scope.query = {
    order: 'name',
    limit: 5,
    page: 1
  };

  $scope.getUsers = function () {
    $scope.promise = User.getUsers();
  };

  $scope.querySearch = querySearch;
  $scope.transformChip = transformChip;
  $scope.onAdd = onAdd;
  $scope.onRemove = onRemove;
  $scope.queries = [];

  function onAdd(chip) {
    if (chip) {
      setUsers(User.filter(chip, $scope.users.data));
    }
  }

  function onRemove() {
    setUsers(local);
  }

  function setUsers(results) {
    $scope.users = {
      count: results.length,
      data: results
    };
  }

  function transformChip(chip) {
    if (angular.isObject(chip)) {
      return chip;
    }
    return {
      type: 'Name',
      value: chip
    };
  }

  function querySearch(query) {
    if (query) {
      return [{
        type: 'Name',
        value: query
      }, {
        type: 'Email',
        value: query
      }, {
        type: 'Favorite Fruit',
        value: query
      }, {
        type: 'Friends',
        value: query
      }];
    } else {
      return [];
    }
  }
}]);