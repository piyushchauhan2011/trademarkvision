'use strict';

describe('myApp.shared module', function() {
  beforeEach(module('myApp.shared'));

  describe('join filter', function() {
    var $filter;

    beforeEach(inject(function(_$filter_) {
      $filter = _$filter_;
    }));

    it('should join name correctly', function() {
      var join = $filter('join');
      expect(join({last:'Chauhan',first:'Piyush'})).toEqual('Piyush Chauhan');
    });
  });
});
