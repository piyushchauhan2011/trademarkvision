'use strict';

angular.module('myApp.shared.join-filter', [])

.filter('join', function() {
  return function(name) {
    return name.first + ' ' + name.last;
  };
});
